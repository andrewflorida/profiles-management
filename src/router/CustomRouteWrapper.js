import React from 'react';
import { Route, Switch, Redirect } from 'react-router-dom';

import RouteWrapper from './RouteWrapper';
import Profiles from '../components/Profiles';
import ProfileForm from '../components/ProfileForm';
import SummaryProfiles from '../components/SummaryProfile';
import SummaryProfileContextProvider from '../contexts/SummaryProfileContext';

export const mapRoutes = items => items.map(route => (
  <RouteWrapper key={route.title} {...route} />
));

const CustomRouteWrapper = () => (
  <Switch>
    <Route path="/profiles/:id">
      <SummaryProfileContextProvider>
        <SummaryProfiles />
      </SummaryProfileContextProvider>
    </Route>
    <Route path="/profiles">
      <Profiles />
    </Route>
    <Route path="/create">
      <ProfileForm title="Create Profile" />
    </Route>
    <Redirect to="/profiles" />
  </Switch>
);

export default CustomRouteWrapper;
