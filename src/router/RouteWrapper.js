import React from 'react';
import PropTypes from 'prop-types';
import { Route } from 'react-router';

const RouteWrapper = ({
  path, exact, component,
}) => (
  <Route
    path={path}
    exact={exact}
    component={component}
  />
);

RouteWrapper.defaultProps = {
  exact: false,
  component: null,
};

RouteWrapper.propTypes = {
  path: PropTypes.string.isRequired,
  exact: PropTypes.bool,
  component: PropTypes.oneOfType([
    PropTypes.node,
    PropTypes.func,
    PropTypes.element,
    PropTypes.object,
  ]),
};

export default RouteWrapper;
