const theme = {
  textColor: '#092621',
  font: '"Roboto", "Helvetica", "Arial", sans-serif;',
  primary: '#3f51b5',
  secondary: '#cbfff6',
  borderRadius: '15px',
  buttonPadding: '10px',
  gradient: 'linear-gradient(#091236, #1E215D)',
  fontFamily: '"Roboto", "Helvetica", "Arial", sans-serif',
};

export default theme;
