import React, { createContext, useEffect, useState } from 'react';
import PropTypes from 'prop-types';
import { isEmpty } from 'lodash';

import {
  createDB, openDB, transaction, getAllObjectData, addObjectData,
} from '../lib/idb';
import suspender from '../lib/suspender';

const setUpDataBase = async () => {
  return createDB('ProfilesDB', 1, {
    name: 'profiles',
    config: { keyPath: 'id', autoIncrement: true },
  });
};

export const ProfilesContext = createContext();

const getAllProfilesFromDB = async () => {
  await setUpDataBase();
  const db = await openDB('ProfilesDB', 1);

  const profilesStore = await transaction(db, 'profiles', 'readwrite')
    .getStore('profiles');

  return getAllObjectData(profilesStore);
};

const resource = suspender(getAllProfilesFromDB());

const ProfilesContextProvider = ({ children }) => {
  const profiles = resource.data.read();

  const [profilesList, setProfilesList] = useState(profiles);

  useEffect(() => {
    if (isEmpty(profilesList)) {
      console.log('List Is empty');
      addObjectData({
        name: 'Phillip',
        creationDate: new Date(),
        balance: 3000,
        percentUsage: 70,
        status: 'Paused',
      });
      addObjectData({
        name: 'Arkadi',
        creationDate: new Date(),
        balance: 5000,
        percentUsage: 30,
        status: 'Active',
      }, { callBack: setProfilesList });
    }
  }, []);

  return (
    <ProfilesContext.Provider value={{ profilesList, setProfilesList }}>
      {children}
    </ProfilesContext.Provider>
  );
};

ProfilesContextProvider.propTypes = {
  children: PropTypes.element.isRequired,
};

export default ProfilesContextProvider;
