import React, { createContext, useEffect, useState } from 'react';
import PropTypes from 'prop-types';

import { useParams } from 'react-router-dom';
import { getObjectById, openDB, transaction } from '../lib/idb';

export const SummaryProfileContext = createContext();

const getUserById = async (id) => {
  const db = await openDB('ProfilesDB', 1);
  const profilesStore = await transaction(db, 'profiles', 'readwrite')
    .getStore('profiles');

  return getObjectById(profilesStore, id);
};

const SummaryProfileContextProvider = ({ children }) => {
  const { id } = useParams();
  const [summaryProfile, setSummaryProfile] = useState(null);

  useEffect(() => {
    if (id) {
      getUserById(Number(id)).then(
        res => setSummaryProfile(res),
      );
    }
  }, []);

  return (
    <SummaryProfileContext.Provider value={{ summaryProfile }}>
      {children}
    </SummaryProfileContext.Provider>
  );
};

SummaryProfileContextProvider.propTypes = {
  children: PropTypes.element.isRequired,
};

export default SummaryProfileContextProvider;
