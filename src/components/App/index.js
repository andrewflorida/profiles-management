import React, { Suspense } from 'react';

import ProfilesContextProvider from '../../contexts/ProfilesContext';
import CustomRouteWrapper from '../../router/CustomRouteWrapper';
import Header from '../Header';

function App() {
  return (
    <Suspense fallback={<p>Loading...</p>}>
      <ProfilesContextProvider>
        <>
          <Header />
          <CustomRouteWrapper />
        </>
      </ProfilesContextProvider>
    </Suspense>
  );
}

export default App;
