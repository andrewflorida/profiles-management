import React from 'react';
import styled from 'styled-components';

import StyledLink from '../StyledLink';

const Div = styled.div`
  display: flex;
  border-bottom: 1px solid ${({ theme }) => theme.primary};
  & a {
    border: 1px solid ${({ theme }) => theme.primary};
    width: 150px;
    margin: 0 5px;
    border-bottom-color: transparent;
    border-top-left-radius: ${({ theme }) => theme.borderRadius};
    border-top-right-radius: ${({ theme }) => theme.borderRadius};
    padding: ${({ theme }) => theme.buttonPadding};
    
    &:hover {
      text-decoration: none;
    }

    @media (max-width: 400px) {
      max-width: 100px;
      font-size: 16px;
    }
  }
`;

const Navigation = () => (
  <Div>
    <StyledLink path="/profiles" title="Profiles" />
    <StyledLink path="/profiles/1" title="Profile Summary" />
    <StyledLink path="/create" title="Create" />
  </Div>
);

export default Navigation;
