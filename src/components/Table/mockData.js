const mockData = [{
  balance: 4000,
  creationDate: 'Tue Mar 23 2021 15:18:55 GMT+0200 (Eastern European Standard Time)',
  id: 2,
  name: 'Nazar',
  percentUsage: 30,
  status: 'Active',
}, {
  balance: 3500,
  creationDate: 'Tue Mar 23 2021 15:18:55 GMT+0200 (Eastern European Standard Time)',
  id: 3,
  name: 'Andrew',
  percentUsage: 30,
  status: 'Active',
}, {
  balance: 4000,
  creationDate: 'Tue Mar 23 2021 15:18:55 GMT+0200 (Eastern European Standard Time)',
  id: 4,
  name: 'Alex',
  percentUsage: 30,
  status: 'Active',
}];

export default mockData;
