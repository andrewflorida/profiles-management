import React, { useState, useEffect, useMemo } from 'react';
import BootstrapTable from 'react-bootstrap-table-next';
import PropTypes from 'prop-types';
import filterFactory from 'react-bootstrap-table2-filter';
import styled from 'styled-components';

import mockData from './mockData';
import SimpleModal from '../SimpleModal';

const Div = styled.div`
  color: ${({ theme }) => theme.textColor};
  
  .react-bootstrap-table {
    overflow: auto;
    text-align: center;
    max-height: calc(100vh - 120px);
  }

  & tbody {
     & tr {
       height: 50px;
       
       &:nth-child(odd){
         background: ${({ theme }) => theme.secondary};
      }
    }
  }
`;

const Table = ({
  data,
  columns,
  defaultSorted,
  ...props
}) => {
  const [showingColumns, setShowingColumns] = useState(columns);

  const updatingColumns = useMemo(() => showingColumns, [showingColumns]);

  const changeColumns = (changedColumns) => {
    setShowingColumns(changedColumns);
  };

  useEffect(() => {
    localStorage.setItem('hiddenColumns', JSON.stringify(showingColumns.filter(i => i?.hidden).map(i => i?.dataField)));
  }, [showingColumns]);

  return (
    <Div>
      <SimpleModal title="Select Columns" onApply={changeColumns} modalTitle="Select Columns" columns={showingColumns} />
      <BootstrapTable
        keyField="id"
        id="list-table"
        data={data}
        columns={updatingColumns}
        noDataIndication="There are no items yet"
        stripped
        condensed
        defaultSorted={defaultSorted}
        filter={filterFactory()}
        {...props}
      />
    </Div>
  );
};

Table.defaultProps = {
  data: mockData,
};

Table.propTypes = {
  data: PropTypes.arrayOf(PropTypes.shape({})),
  defaultSorted: PropTypes.arrayOf(PropTypes.shape({})).isRequired,
  columns: PropTypes.arrayOf(PropTypes.shape({})).isRequired,
};

export default Table;
