import React, { useEffect, useState } from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';
import Image from 'material-ui-image';
import avatar from '../../../assets/avatar.png';

const Avatar = styled.img`
  opacity: 0.6;
  width: 100%;
  height: auto;
`;

const AvatarFormatter = ({ item }) => {
  const [imagePreviewUrl, setUrl] = useState(undefined);

  useEffect(() => {
    if (item) {
      const reader = new FileReader();
      reader.readAsDataURL(item);

      reader.onloadend = () => {
        setUrl(reader.result);
      };
    }
  }, [item]);

  // eslint-disable-next-line jsx-a11y/alt-text
  return <Image imageStyle={{ objectFit: 'cover' }} src={imagePreviewUrl} alt="avatar" loading={<Avatar src={avatar} alt="avatar" />} />;
};

AvatarFormatter.defaultProps = {
  item: undefined,
};

AvatarFormatter.propTypes = {
  item: PropTypes.instanceOf(File),
};

export default AvatarFormatter;
