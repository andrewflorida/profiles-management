const currencyFormatter = (num, currency = '$') => `${num}${currency}`;

export default currencyFormatter;
