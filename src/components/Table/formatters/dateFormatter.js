import moment from 'moment';

const dateFormatter = date => moment(date).format('DD/MM/YYYY');

export default dateFormatter;
