import React from 'react';
import styled from 'styled-components';
import dateFormatter from './formatters/dateFormatter';
import AvatarFormatter from './formatters/AvatarFormatter';
import StyledLink from '../StyledLink';
import percentFormatter from './formatters/percentFormatter';

const Div = styled.div`
  width: 150px;
`;

const profilesColumns = [{
  dataField: 'name',
  text: 'Profile name',
  sort: true,
  formatter: (cell, data) => <StyledLink title={cell} path={`/profiles/${data.id}`} withIcon />,
  headerStyle: () => ({ width: '18%' }),
}, {
  dataField: 'status',
  text: 'Profile status',
  sort: true,
  headerStyle: () => ({ width: '18%' }),
}, {
  dataField: 'creationDate',
  text: 'Creation Date',
  sort: true,
  formatter: dateFormatter,
  headerStyle: () => ({ width: '18%' }),
}, {
  dataField: 'avatar',
  text: 'Avatar',
  sort: true,
  formatter: item => (<Div><AvatarFormatter item={item} /></Div>),
  headerStyle: () => ({ width: '0.1%' }),
}, {
  dataField: 'percentUsage',
  text: 'Usage, %',
  sort: true,
  formatter: percentFormatter,
  headerStyle: () => ({ width: '18%' }),
}, {
  dataField: 'balance',
  text: 'Balance, $',
  sort: true,
  hidden: true,
  headerStyle: () => ({ width: '18%' }),
}];

export default profilesColumns;
