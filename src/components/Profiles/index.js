import React, { useContext } from 'react';

import Table from '../Table';
import profilesColumns from '../Table/columns';
import { ProfilesContext } from '../../contexts/ProfilesContext';

function Profiles() {
  const { profilesList } = useContext(ProfilesContext);
  const hiddenColumns = JSON.parse(localStorage.getItem('hiddenColumns'));
  const localStorageSort = JSON.parse(localStorage.getItem('sorted'));
  const defaultSorted = localStorageSort || [{ dataField: 'name', order: 'asc' }];

  const onSort = (dataField, order) => {
    localStorage.setItem('sorted', JSON.stringify([{ dataField, order }]));
  };

  const storageColumns = hiddenColumns
  // eslint-disable-next-line max-len
    ? profilesColumns.map(column => ({ ...column, hidden: hiddenColumns.includes(column?.dataField) }))
    : profilesColumns;

  const addSortToStorageColumns = storageColumns.map(column => ({ ...column, onSort }));

  return (
    <Table columns={addSortToStorageColumns} data={profilesList} defaultSorted={defaultSorted} />
  );
}

export default Profiles;
