import React from 'react';
import PropTypes from 'prop-types';
import { map } from 'lodash';
import InputsWrapper from './InputsWrapper';
import withFormikWrapper from './withFormikWrapper';

const UniversalForm = ({
  values,
  errors,
  handleChange,
  handleBlur,
  fields,
  lists,
  handleSubmit,
  formId,
  classes,
  additionalHandler,
}) => {
  return (
    <form
      id={formId}
      onSubmit={handleSubmit}
    >
      {map(fields, ({
        id,
        name,
        label,
        type,
        multiple,
        disabled,
      }) => {
        const key = name;
        const value = values[name];
        const errorText = errors[name];
        let dropdownList = [];
        if (lists) {
          dropdownList = lists[id];
        }
        return (
          <InputsWrapper
            id={id}
            key={key}
            value={value}
            error={errorText}
            name={name}
            label={label}
            type={type}
            handleChange={handleChange}
            handleBlur={handleBlur}
            dropdownList={dropdownList}
            classes={classes}
            multiple={multiple}
            disabled={disabled}
            additionalHandler={additionalHandler}
          />
        );
      })}
    </form>
  );
};

UniversalForm.defaultProps = {
  errors: {},
  additionalHandler: null,
  lists: {},
};

UniversalForm.propTypes = {
  additionalHandler: PropTypes.func,
  formId: PropTypes.string.isRequired,
  values: PropTypes.objectOf(PropTypes.any).isRequired,
  errors: PropTypes.objectOf(PropTypes.any),
  handleChange: PropTypes.func.isRequired,
  handleBlur: PropTypes.func.isRequired,
  handleSubmit: PropTypes.func.isRequired,
  fields: PropTypes.arrayOf(PropTypes.object).isRequired,
  classes: PropTypes.objectOf(PropTypes.oneOfType([
    PropTypes.object,
    PropTypes.string,
    PropTypes.func,
    PropTypes.number,
  ])).isRequired,
  lists: PropTypes.shape({}),
};

export default withFormikWrapper(UniversalForm);
