import * as yup from 'yup';

export const formData = [
  {
    id: 'name',
    name: 'name',
    label: 'Profile Name',
    type: 'text',
  },
  {
    id: 'status',
    name: 'status',
    label: 'Profile Status',
    type: 'dropdown',
  },
  {
    id: 'percentUsage',
    name: 'percentUsage',
    label: 'Usage, %',
    type: 'number',
  },
  {
    id: 'balance',
    name: 'balance',
    label: 'Balance, $',
    type: 'number',
  },
  {
    id: 'avatar',
    name: 'avatar',
    label: 'Avatar',
    type: 'file',
  },
];

const FILE_SIZE = 300 * 1024;
const SUPPORTED_FORMATS = [
  'image/jpg',
  'image/jpeg',
  'image/webp',
  'image/png',
];

export const validationSchema = yup.object({
  name: yup.string().required('Required!'),
  percentUsage: yup.number().test('%', 'Min 0, max 100', value => value <= 100 && value >= 0).required('Required!'),
  balance: yup.number().test('balance', 'Min 0, max 100,000', value => value <= 10000 && value >= 0).required('Required!'),
  avatar: yup.mixed().test(
    'fileRequired',
    'File is required',
    value => value,
  ).test(
    'fileSize',
    'File too large, 300kb max',
    value => value && value.size <= FILE_SIZE,
  ).test(
    'fileFormat',
    'Unsupported Format, only jpg, jpeg, png, webp',
    value => value && SUPPORTED_FORMATS.includes(value.type),
  )
    .required('Required!'),
});
