import React from 'react';
import '@testing-library/jest-dom/extend-expect';
import { render } from '@testing-library/react';
import InputDropdown from './index';

test('renders content', () => {
  const items = [
    { id: 'Active', name: 'Active' },
    { id: 'Paused', name: 'Paused' },
    { id: 'Deleted', name: 'Deleted' },
  ];

  const component = render(
    <InputDropdown items={items} onChange={console.log} selected="Active" label="test" />,
  );

  expect(component.container).toHaveTextContent(
    'Active',
  );
});
