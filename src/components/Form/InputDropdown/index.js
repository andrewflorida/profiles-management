import React from 'react';
import PropTypes from 'prop-types';
import { get, find, map } from 'lodash';
import InputLabel from '@material-ui/core/InputLabel';
import MenuItem from '@material-ui/core/MenuItem';
import Select from '@material-ui/core/Select';
import { InputContainer } from '../../../elements/formElemetns';

const InputDropdown = ({
  items,
  onChange,
  selected,
  label,
}) => {
  const defaultText = label || 'Select value';
  const selectedItem = get(find(items, { id: selected }), 'name', defaultText);

  const handleChange = (event) => {
    onChange(event.target.value);
  };

  return (
    <InputContainer>
      <InputLabel id="select-label">{label}</InputLabel>
      <Select
        labelId="select-label"
        id="select"
        value={selectedItem}
        onChange={handleChange}
      >
        {
          map(items, ({ name }, i) => (
            <MenuItem key={i} value={name}>{name}</MenuItem>
          ))
        }
      </Select>
    </InputContainer>
  );
};

InputDropdown.propTypes = {
  items: PropTypes.arrayOf(PropTypes.object).isRequired,
  onChange: PropTypes.func.isRequired,
  label: PropTypes.string.isRequired,
  selected: PropTypes.oneOfType([
    PropTypes.string,
    PropTypes.number,
  ]).isRequired,
};

export default InputDropdown;
