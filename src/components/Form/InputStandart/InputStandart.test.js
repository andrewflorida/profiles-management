import React from 'react';
import '@testing-library/jest-dom/extend-expect';
import { render } from '@testing-library/react';
import InputStandart from './index';

test('renders content', () => {
  const items = {
    id: '1',
    name: 'test',
    value: 'testVal',
    label: 'label',
  };

  const component = render(
    <InputStandart {...items} />,
  );

  expect(component.container).toHaveTextContent(
    'label',
  );
});
