import React from 'react';
import PropTypes from 'prop-types';
import TextField from '@material-ui/core/TextField';
import { InputContainer } from '../../../elements/formElemetns';

const InputStandart = ({
  type,
  id,
  name,
  value: initValue,
  onChange,
  label,
  disabled,
}) => {
  const handleChange = ({ target: { value, files } }) => {
    const newValue = type === 'file' ? files[0] : value;
    onChange(newValue);
  };

  const renderInput = () => {
    if (type === 'file') {
      return (
        <>
          {label}
          <input
            id="upload-photo"
            name="upload-photo"
            type={type}
            onChange={handleChange}
          />
        </>
      );
    }
    return (
      <InputContainer>
        <TextField
          id={id}
          label={label}
          name={name}
          type={type}
          value={initValue}
          onChange={handleChange}
          disabled={disabled}
        />
      </InputContainer>
    );
  };
  return (
    <>
      {renderInput()}
    </>
  );
};

InputStandart.defaultProps = {
  value: '',
  onChange: null,
  id: '',
  type: 'text',
  label: '',
  disabled: false,
};
InputStandart.propTypes = {
  // eslint-disable-next-line max-len
  value: PropTypes.oneOfType([PropTypes.string, PropTypes.number, PropTypes.object]),
  onChange: PropTypes.func,
  id: PropTypes.string,
  type: PropTypes.string,
  name: PropTypes.string.isRequired,
  label: PropTypes.string,
  disabled: PropTypes.bool,
};

export default InputStandart;
