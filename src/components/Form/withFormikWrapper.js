import { withFormik } from 'formik';
import { isEmpty, transform } from 'lodash';

const withFormikWrapper = withFormik({
  enableReinitialize: true,
  mapPropsToValues: ({ propsData, fields }) => {
    const defaultValue = (type) => {
      switch (type) {
        case 'text':
        case 'file':
          return '';
        case 'number': return 0;
        default: return '';
      }
    };

    const propsValue = (fieldName, type) => {
      if (propsData[fieldName] || propsData[fieldName] === 0) {
        return propsData[fieldName];
      }
      return defaultValue(type);
    };
    return transform(fields, (result, field) => {
      const { name, type } = field;
      result[name] = isEmpty(propsData)
        ? defaultValue(type)
        : propsValue(name, type);
    }, {});
  },
  handleSubmit(values, { props: { onSubmit } }) {
    onSubmit(values);
  },
  validateOnChange: false,
  validateOnBlur: false,
  validationSchema: ({ validationSchema }) => validationSchema,
});

export default withFormikWrapper;
