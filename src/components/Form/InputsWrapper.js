import React from 'react';
import PropTypes from 'prop-types';
import FormHelperText from '@material-ui/core/FormHelperText';
import InputStandart from './InputStandart';
import InputDropdown from './InputDropdown';

const InputsWrapper = ({
  id,
  label,
  value,
  error,
  name,
  handleChange,
  type,
  additionalHandler,
  dropdownList,
  disabled,
}) => {
  const key = name;
  const handleChangeValidation = (currentValue) => {
    if (additionalHandler) {
      additionalHandler({ name, value: currentValue });
    }
    handleChange({ target: { name, value: currentValue } });
  };

  const renderInput = () => {
    switch (type) {
      case 'dropdown':
        return (
          <InputDropdown
            key={key}
            name={name}
            label={label}
            selected={value}
            items={dropdownList}
            onChange={handleChangeValidation}
          />
        );
      default:
        return (
          <InputStandart
            error={error}
            type={type}
            id={id}
            name={name}
            value={value}
            onChange={handleChangeValidation}
            label={label}
            disabled={disabled}
          />
        );
    }
  };
  return (
    <div>
      {renderInput()}
      <FormHelperText>{error}</FormHelperText>
    </div>
  );
};

InputsWrapper.defaultProps = {
  id: '',
  value: '',
  error: '',
  handleChange: null,
  type: 'text',
  dropdownList: null,
  additionalHandler: null,
  disabled: false,
};

InputsWrapper.propTypes = {
  id: PropTypes.string,
  label: PropTypes.string.isRequired,
  value: PropTypes.oneOfType([
    PropTypes.string,
    PropTypes.number,
    PropTypes.object,
    PropTypes.bool,
    PropTypes.array,
  ]),
  error: PropTypes.string,
  name: PropTypes.string.isRequired,
  handleChange: PropTypes.func,
  additionalHandler: PropTypes.func,
  type: PropTypes.string,
  dropdownList: PropTypes.arrayOf(PropTypes.object),
  disabled: PropTypes.bool,
};

export default InputsWrapper;
