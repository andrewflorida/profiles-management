import { Button } from '@material-ui/core';
import styled from 'styled-components';

export const StyledButton = styled(Button)`
  color: ${({ theme }) => theme.color}!important;
  text-decoration: none;
  padding: 6px 8px;
  border: 1px solid ${({ theme }) => theme.color}!important;
  min-width: 70px!important;
  margin: 5px!important;
`;

export default StyledButton;
