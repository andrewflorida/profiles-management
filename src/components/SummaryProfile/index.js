import React, { useContext } from 'react';

import Grid from '@material-ui/core/Grid';
import CardActionArea from '@material-ui/core/CardActionArea';
import CardContent from '@material-ui/core/CardContent';
import Typography from '@material-ui/core/Typography';

import { Container } from './SummaryProfileStyled';
import AvatarFormatter from '../Table/formatters/AvatarFormatter';
import dateFormatter from '../Table/formatters/dateFormatter';
import percentFormatter from '../Table/formatters/percentFormatter';
import currencyFormatter from '../Table/formatters/currencyFormatter';
import { SummaryProfileContext } from '../../contexts/SummaryProfileContext';

const SummaryProfiles = () => {
  const { summaryProfile } = useContext(SummaryProfileContext);

  if (!summaryProfile) {
    return (
      <span> </span>
    );
  }

  return (
    <Grid
      container
      direction="column"
    >
      <h2>Profile</h2>
      <Container>
        <CardActionArea>
          <AvatarFormatter item={summaryProfile.avatar} />
          <CardContent>
            <Typography gutterBottom variant="h5" component="h2">
              {summaryProfile.name}
            </Typography>
            <Typography variant="body2" color="textSecondary" component="p">
              <p>{`Balance: ${currencyFormatter(summaryProfile.balance)}`}</p>
            </Typography>
            <Typography variant="body2" color="textSecondary" component="p">
              <p>{`Status: ${summaryProfile.status}`}</p>
            </Typography>
            <Typography variant="body2" color="textSecondary" component="p">
              <p>{`Percent Usage: ${percentFormatter(summaryProfile.percentUsage)}`}</p>
            </Typography>
            <Typography variant="body2" color="textSecondary" component="p">
              <p>{`Create Date: ${dateFormatter(summaryProfile.date)}`}</p>
            </Typography>
          </CardContent>
        </CardActionArea>
      </Container>
    </Grid>
  );
};

export default SummaryProfiles;
