import styled from 'styled-components';

import CardMedia from '@material-ui/core/CardMedia';
import Card from '@material-ui/core/Card';

export const Container = styled(Card)`
  width: 400px;
`;

export const Image = styled(CardMedia)`
  width: 100px;
`;
