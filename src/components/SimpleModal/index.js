import React, { useState, useEffect } from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';
import { isEmpty } from 'lodash';
import {
  Checkbox, FormControlLabel, TextField, Modal,
} from '@material-ui/core';

// eslint-disable-next-line import/no-named-as-default
import StyledButton from '../StyledButton';

const ContentWrapper = styled.div`
  position: absolute;
  display: flex;
  flex-direction: column;
  justify-content: space-between;
  width: 400px;
  min-height: 450px;
  background-color: #fff;
  border: 2px solid #000;
  padding: 15px;
  top: 50%;
  left: 50%;
  transform: translate(-50%, -50%);

  @media (max-width: 450px) {
    max-width: 300px;
  }
`;

const StyledTitle = styled.p`
  display: block;
  font-weight: 400;
  line-height: 1.5;
  border-bottom: 1px solid;
  font-size: 18px;
`;

const ButtonsBlock = styled.div`
  display: block;
`;

const SearchBlock = styled.div`
  display: block;
`;

const ItemsBlock = styled.div`
  display: flex;
  flex-direction: column;
  flex: 2;
`;

const SimpleModal = ({
  title, onApply, modalTitle, columns,
}) => {
  const [open, setOpen] = useState(false);
  const [searchVal, setSearchVal] = useState('');
  const [showingColumns, setShowingColumns] = useState(columns);

  const toggleColumn = (dataField, isHidden) => () => {
    // eslint-disable-next-line max-len
    const newColumns = showingColumns.map(i => (i.dataField === dataField ? { ...i, hidden: isHidden } : i));
    setShowingColumns(newColumns);
  };

  const handleOpen = () => {
    setOpen(true);
  };

  const handleClose = () => {
    setOpen(false);
  };

  const handleSearch = (e) => {
    setSearchVal(e.target.value);
  };

  const handleApply = () => {
    onApply(showingColumns);
    handleClose();
  };

  useEffect(() => () => {
    setSearchVal('');
  }, []);

  return (
    <div>
      <StyledButton onClick={handleOpen}>
        { title }
      </StyledButton>
      <Modal
        open={open}
        onClose={handleClose}
        aria-labelledby="simple-modal-title"
        aria-describedby="simple-modal-description"
      >
        <ContentWrapper>
          <StyledTitle>{modalTitle}</StyledTitle>
          <SearchBlock>
            <TextField id="standard-basic" label="Search Columns..." onChange={handleSearch} />
          </SearchBlock>
          <ItemsBlock>
            {showingColumns.map(({ dataField, text, hidden = false }) => (
              (isEmpty(searchVal) || text.includes(searchVal)) && (
                <FormControlLabel
                  key={dataField}
                  control={(
                    <Checkbox
                      disabled={dataField === 'name'}
                      checked={!hidden}
                      name={dataField}
                      onChange={toggleColumn(dataField, !hidden)}
                    />
                  )}
                  label={text}
                />
              )
            ))}
          </ItemsBlock>
          <ButtonsBlock>
            <StyledButton onClick={handleApply}>Apply</StyledButton>
            <StyledButton onClick={handleClose}>Cancel</StyledButton>
          </ButtonsBlock>
        </ContentWrapper>
      </Modal>
    </div>
  );
};

SimpleModal.propTypes = {
  title: PropTypes.string.isRequired,
  modalTitle: PropTypes.string.isRequired,
  onApply: PropTypes.func.isRequired,
  columns: PropTypes.arrayOf(PropTypes.shape({})).isRequired,
};

export default SimpleModal;
