import styled from 'styled-components';

import Card from '@material-ui/core/Card';
import { Grid } from '@material-ui/core';

export const ImageContainer = styled(Grid)`
  width: 250px;
  margin-left: 25px;
`;

export const Container2 = styled(Card)`
  width: 400px;
`;
