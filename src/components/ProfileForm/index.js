import React, { useState, useContext } from 'react';
import { useHistory } from 'react-router-dom';
import PropTypes from 'prop-types';
import { Grid } from '@material-ui/core';

import UniversalForm from '../Form/UniversalForm';
import { validationSchema, formData } from '../Form/formData';
// eslint-disable-next-line import/no-named-as-default
import StyledButton from '../StyledButton';
import { ProfilesContext } from '../../contexts/ProfilesContext';
import { addObjectData } from '../../lib/idb';
import AvatarFormatter from '../Table/formatters/AvatarFormatter';
import { ImageContainer } from './ProfileFormStyled';

const dropDownData = {
  status: [
    { id: 'Active', name: 'Active' },
    { id: 'Paused', name: 'Paused' },
    { id: 'Deleted', name: 'Deleted' },
  ],
};

function ProfileForm({ propsData, title }) {
  const [formInput, setFormInput] = useState(propsData);
  const history = useHistory();
  const formId = 'profile-form';

  const additionalHandler = ({ name, value }) => {
    setFormInput(state => ({ ...state, [name]: value }));
  };

  const { setProfilesList } = useContext(ProfilesContext);

  const handleSubmit = async (values) => {
    try {
      await addObjectData(values, { callBack: setProfilesList });
      history.push('/profiles');
    } catch (e) {
      console.log(e);
    }
  };

  return (
    <Grid container direction="column">
      <h2>{title}</h2>
      <Grid container>
        <UniversalForm
          validationSchema={validationSchema}
          fields={formData}
          onSubmit={handleSubmit}
          formId={formId}
          withLabel
          propsData={formInput}
          lists={dropDownData}
          additionalHandler={additionalHandler}
        />
        <ImageContainer>
          <AvatarFormatter item={formInput.avatar} />
        </ImageContainer>
      </Grid>
      <Grid item>
        <StyledButton variant="contained" color="primary" form={formId} type="submit">
          Save
        </StyledButton>
      </Grid>
    </Grid>
  );
}

ProfileForm.defaultProps = {
  propsData: {
    name: '',
    status: 'Active',
    percentUsage: 0,
    balance: 0,
    avatar: '',
  },
};

ProfileForm.propTypes = {
  propsData: PropTypes.objectOf(PropTypes.oneOfType([
    PropTypes.object,
    PropTypes.string,
    PropTypes.array,
    PropTypes.number,
  ])),
  title: PropTypes.string.isRequired,
};

export default ProfileForm;
