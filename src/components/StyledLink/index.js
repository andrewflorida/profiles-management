import React from 'react';
import { Link } from 'react-router-dom';
import styled from 'styled-components';
import PropTypes from 'prop-types';
import OpenInNewIcon from '@material-ui/icons/OpenInNew';

export const Styled = styled(Link)`
  display: flex;
  justify-content: center;
  align-items: center;
  color: ${({ theme }) => theme.color};
  margin: 1em;
  text-decoration: none;
`;

const StyledLink = ({ path, title, withIcon }) => {
  return (
    <Styled to={path}>
      <span>{title}</span>
      {withIcon && <OpenInNewIcon />}
    </Styled>
  );
};

StyledLink.defaultProps = {
  withIcon: false,
};

StyledLink.propTypes = {
  path: PropTypes.string.isRequired,
  title: PropTypes.string.isRequired,
  withIcon: PropTypes.bool,
};

export default StyledLink;
