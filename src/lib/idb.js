export const createDB = (name, version, store) => {
  if (!('indexedDB' in window)) {
    throw new Error('IndexedDB is not supported in this browser!');
  }

  return new Promise((resolve, reject) => {
    const openDBRequest = indexedDB.open(name, version);

    openDBRequest.onupgradeneeded = (event) => {
      const db = event.target.result;

      if (store && !db.objectStoreNames.contains(store.name)) {
        db.createObjectStore(store.name, store.config);
      }
    };

    openDBRequest.onsuccess = (event) => {
      const db = event.target.result;

      resolve(db);
    };

    openDBRequest.onerror = (event) => {
      const { error } = event.target;

      reject(error);
    };
  });
};

export const openDB = (name, version) => {
  if (!('indexedDB' in window)) {
    throw new Error('IndexedDB is not supported in this browser!');
  }

  return new Promise((resolve, reject) => {
    const openDBRequest = indexedDB.open(name, version);

    openDBRequest.onsuccess = (event) => {
      const db = event.target.result;

      resolve(db);
    };

    openDBRequest.onerror = (event) => {
      const { error } = event.target;

      reject(error);
    };
  });
};

export function transaction(dbObject, store, mode) {
  return {
    tx: dbObject.transaction(store, mode),
    getStore(name) {
      return new Promise((resolve, reject) => {
        const tx = dbObject.transaction(store, mode);

        const objStore = tx.objectStore(name);

        resolve(objStore);

        tx.onerror = (event) => {
          reject(event.target.error);
        };
      });
    },
  };
}

export const getObjectById = (store, objectKeyPath) => {
  return new Promise((resolve, reject) => {
    const dataRequest = store.get(objectKeyPath);

    dataRequest.onsuccess = (event) => {
      resolve(event.target.result);
    };

    dataRequest.onerror = (event) => {
      reject(event.target.error);
    };
  });
};

export const getAllObjectData = (store) => {
  return new Promise((resolve, reject) => {
    const dataRequest = store.getAll();

    dataRequest.onsuccess = (event) => {
      resolve(event.target.result);
    };

    dataRequest.onerror = (event) => {
      reject(event.target.error);
    };
  });
};

export const addObjectData = async (dataBody, {
  dbName = 'ProfilesDB',
  tableName = 'profiles',
  callBack = null,
} = {}) => {
  const db = await openDB(dbName, 1);

  const profilesStore = await transaction(db, tableName, 'readwrite').getStore(tableName);

  const dataRequest = await profilesStore.add(dataBody);

  dataRequest.onsuccess = () => {
    if (callBack) {
      getAllObjectData(profilesStore).then((storeData) => callBack(storeData));
    }
    return ({
      message: 'Success',
    });
  };

  dataRequest.onerror = (event) => {
    const { error } = event.target;
    return ({
      error,
    });
  };
};
